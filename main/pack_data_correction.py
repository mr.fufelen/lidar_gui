# py 3.10.0 64-bit


def check_pack(max_val_in_pack, uart_list):
# если значение превышает максимальное - попытка MSB to LSB

    new_uart_list = list()
    for val in uart_list:
        if val > max_val_in_pack:
            val = ((val & 0x00FF) << 8) | ((val & 0xFF00) >> 8) # msb to lsb    
            new_uart_list.append(val)
    return new_uart_list
    



if __name__ == "__main__":

    test = [11008, 31490, 65024]

    test = check_pack(254, test)

    print("modul's debug")
    a = 0