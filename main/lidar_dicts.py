
dict_set_headers ={
    # словарь с заголовками установочных команд

    "setMotorSpeedRPM"					    : 0x4440, # установка скорости вращения зеркала лидара
    "setVoltageOnPowerSupply12_to_60"		: 0x4441, # напряжение на импульсном повышающем трансформаторе 12->60 для питания лавинного фотодиода и лазера
    "setTDCvalue"							: 0x4442, # данные импульса (длительность импульса)
    "setPWMdutyPhD"					    	: 0x4443, # коэффициент заполнения ШИМ для "поджига" лавинного фотодиода (tu/T*100%)
    "setPWMdutyLaser"						: 0x4444, # коэффициент заполнения ШИМ для поджига лазера (tu/T*100%)
    "setPWMdutyOnPowerSupply12_to_60"		: 0x4445, # коэффициент заполнения ШИМ для питания лавинного фотодиода и лазера (tu/T*100%)

    }

dict_req_headers ={
    # словарь с заголовками команд запросов
    "reqMotorSpeedRPM"				    :	0x9990, # запрос скорости вращения зеркала лидара
    "reqVoltageOnPowerSupply12_to_60"	:	0x9991, # напряжение на импульсном повышающем трансформаторе 12->60 для питания лавинного фотодиода и лазера
    "reqTDCvalue"						:	0x9992, # данные импульса (длительность импульса)
    "reqPWMdutyPhD"					    :	0x9993, # коэффициент заполнения ШИМ для "поджига" лавинного фотодиода (tu/T*100%)
    "reqPWMdutyLaser"					:	0x9994, # коэффициент заполнения ШИМ для поджига лазера (tu/T*100%)
    "reqPWMdutyOnPowerSupply12_to_60"	:	0x9995, # коэффициент заполнения ШИМ для питания лавинного фотодиода и лазера (tu/T*100%)
    }

dict_resp_headers ={
    # словарь с заголовками команд ответов
    # заголовки, которые могут быть приняты с контроллера
    "mainPackHeader"                        : 0xFFFE,



    "respMotorSpeedRPM"						: 0xAAA0, # установка скорости вращения зеркала лидара
    "respVoltageOnPowerSupply12_to_60"		: 0xAAA1, # напряжение на импульсном повышающем трансформаторе 12->60 для питания лавинного фотодиода и лазера
    "respTDCvalue"							: 0xAAA2, # данные импульса (длительность импульса)
    "respPWMdutyPhD"						: 0xAAA3, # коэффициент заполнения ШИМ для "поджига" лавинного фотодиода (tu/T*100%)
    "respPWMdutyLaser"						: 0xAAA4, # коэффициент заполнения ШИМ для поджига лазера (tu/T*100%)
    "respPWMdutyOnPowerSupply12_to_60"		: 0xAAA5, # коэффициент заполнения ШИМ для питания лавинного фотодиода и лазера (tu/T*100%)
    "respCommandReceived"					: 0xAAA6, # принята команда или нет, обнаружен ли в ней заголовок (для обратной связи)
    "respError"								: 0xAAA7, # вывод возможной ошибки (см. enum)
    }



# dict_with_headers = {
#     # набор заголовков для парсинга пакета
#     'header'            :   0xFFFF,                    
#     'header_test'       :   253,                 
#     'header_graph'      :   0xFFFF - 1,           
#     'header_encoder'    :   0xFFFF - 2,               
#     }

dict_with_terminators = {
    # набор терминаторов для парсинга пакета
        'terminator'        :    0xFF9B,#(65535 - 100),
        'terminator_test'   :    254,
    }

dict_parsed_pack = dict() #{ # питон его сам заполнит

'''
# словарь для хранения распарсенного пакета
'header': -1,
'body_1': -1,
'body_2': -1,
'body_3': -1,
'body_4': -1,
'body_5': -1,
'body_6': -1,
'body_7': -1,
'body_8': -1,
'body_9': -1,
'body_10': -1,
'terminator': -1,


'pos_header':-1,
'pos_body_1': -1,
'pos_body_2': -1,
'pos_body_3': -1,
'pos_body_4': -1,
'pos_body_5': -1,
'pos_body_6': -1,
'pos_body_7': -1,
'pos_body_8': -1,
'pos_body_9': -1,
'pos_body_10': -1,
'pos_terminator': -1,

'packet_validated': -1
'''
    # }

# словарь для работы с пакетами
encoder_packet_struct = {
    'header': 65535,
    'header_graph': (65535 - 1),
    'header_encoder': (65535 - 2),
    # 'header': 253,
    'degree': 0,
    'speed': 0,
    'terminator': (65535 - 100),
    # 'terminator': 254,

    'pos_header':0,
    'pos_degree': 0,
    'pos_speed': 0,
    'pos_terminator': 254,

    'packet_validated': False
    }


if __name__ == "__main__":
    print("lidar_dicts_debug")