import socket

def main():
    sock = socket.socket()
    sock.bind(('', 9090)) # 9090 - порт, '' - сервер доступен для всех интерфейсов

    sock.listen(1)

    conn, addr = sock.accept() # прием параметров клиента

    while True:
        data = conn.recv(1024) # получаем от клиента порцию в 1024 байт
        if not data:
            break
        conn.send(data.upper()) # отправка данных клиенту

    conn.close()
    

if __name__ == '__main__':
    main()