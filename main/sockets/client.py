import sys  # We need sys so that we can pass argv to QApplication

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from ui_mainwindow import Ui_MainWindow
import socket

class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self) # setup от Ui_MainWindow
        self.setWindowTitle("LidarGUI") # название окна


        self.ipserver = '192.168.0.101'
        self.port = 2424
        # # дебажный таймер для отладки парсинга пакета
        # self.timerDebug = QTimer()
        # self.timerDebug.timeout.connect(self.timerPrint)
        # self.timerDebug.start(1000)

        self.show() # показать интерфейс
        # конец конструктора

        self.pbTurnOnOff12V.clicked.connect(lambda: self.sendPackToSocket())

        self.ReleState = False

    def sendPackToSocket(self, NumRele = 0, State = 0):

        sock = socket.socket()

        ipserver = '192.168.0.101'
        port = 2424

        sock.connect((ipserver, port))
        if self.ReleState == 0:
            self.ReleState = 1
        else:
            self.ReleState = 0

        

        sock.send(bytes("$KE,REL,{},{}\r\n".format(1,self.ReleState), 'utf-8'))

        # while True:
        data = sock.recv(1024)
        if not data:
            ...
            # break
        print(data)

        sock.close()
        return

        sock = socket.socket()
        sock.connect((self.ipserver, self.port))
        self.ReleState = not self.ReleState
        sock.send(bytes("$KE,REL,{},{}\r\n".format(1,1), 'utf-8'))
        # sock.send(bytes("$KE,REL,{},{}\r\n".format(1,self.ReleState), 'utf-8'))
        sock.close()
        print("aaaaa")


    def timerPrint(self):
        print("hello_from_timer")
        return
        

def main():
    

    app = QApplication([])
    # app = QtWidgets.QApplication([])

    w = MainWindow()

    # app.exec()
    sys.exit(app.exec_())

# def main():
#     app = QApplication([])
#     sys.exit(app.exec_())

    sock = socket.socket()

    ipserver = '192.168.0.101'
    port = 2424

    sock.connect((ipserver, port))
    
    sock.send(bytes("$KE,REL,{},{}\r\n".format(1,0), 'utf-8'))

    while True:
        data = sock.recv(1024)
        if not data:
            break
        print(data)

    sock.close()


if __name__ == '__main__':
    main()