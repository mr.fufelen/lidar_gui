from random import randint
import numpy as np
import math
import pyqtgraph as pg

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from pyqtgraph import PlotWidget

def map_range(x, in_min, in_max, out_min, out_max):
    # map - Arduino
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def update_SCREENS(ui, encoder_packet_struct):
    # обновление всех "отображалок"
    ui.enc_deg_lcd.display(encoder_packet_struct['degree'])                 # данные для углового положения энкодера
    ui.enc_rpm_lcd.display(encoder_packet_struct['speed'])                  # данные для скорости оборотов
    
    ui.enc_pos_dial.setSliderPosition(70 + encoder_packet_struct['degree'])      # крутилка

def update_plot_data(obj, dict_to_graph):

        radius = dict_to_graph['radius']
        theta = dict_to_graph['theta']

        obj.counter += 1

        # 1. массив углов перевел в массив точек
        # 2. 

        # 0 < theta < 360
        # 0 < mapped_theta_dots < obj.NUM_points
        # mapped_theta_dots = list()
        # for var in obj.uart_theta:
        while(theta > 360):
            theta -= 360
        # изменение значений ячеек массива их соответствующими градусами в полярных координатах
        theta = map_range(theta, 0, 360, 0, (obj.NUM_points - 1))

        try:
            obj.uart_radius[round(theta)] = radius
        except IndexError:
             a = round(theta)
             ...

        # # работа с массивом градусов
        # obj.uart_theta = obj.uart_theta[1:]  # Remove the first y element.
        # obj.uart_theta.append(obj.counter)  # добавление очередного значения на график


        # # работа с массивом радиусов
        # obj.uart_radius = obj.uart_radius[1:]  # Remove the first
        # # obj.uart_radius.append(randint(50,55))  # Add a new random value.

        # obj.uart_radius.append(radius)  # Add a new random value.

        

        polar_graph_plot(obj, obj.uart_radius, obj.uart_theta)
        # obj.graph.plot(obj.uart_x, obj.uart_y)  # Update the data.

def polar_graph_plot(obj, radius, theta):
        # принимает 2 массива и отстраивает их на графике

        # radius = np.random.normal(loc=10, size=1000) # изменяемый набор амплитуд для полярных координат
        # theta = np.linspace(0, 2 * np.pi, 1000) # набор угловых координат для графика
        
        mapped_theta_radians = list()

        for var in theta:
            # изменение значений ячеек массива их соответствующими градусами в полярных координатах
            mapped_theta_radians.append(map_range(var, 0, obj.NUM_points, 0, 2*math.pi))
        
        # Transform to cartesian and plot
        x = radius * np.cos(mapped_theta_radians)
        y = radius * np.sin(mapped_theta_radians)

        pen = pg.mkPen(color=(100, 255, 255))

        obj.graph.clear() # очистка графика для избегания наложения

        # obj.graph.setBackgroundBrush(Qt.GlobalColor.darkGray)
        # obj.graph.setForegroundBrush(Qt.GlobalColor.darkGreen)


        # 0.0 → black
        # 0.2 → red
        # 0.6 → yellow
        # 1.0 → white

        # cm = pg.colormap.get('CET-L17') # prepare a linear color map
        cm = pg.colormap.get('CET-D1') # prepare a diverging color map
        cm.setMappingMode('mirror') # set mapping mode
        pen = cm.getPen(span=(min(x), max(y)), orientation = 'horizontal', width = 1) # gradient from blue (y=0) to white (y=1)

        # brush = cm.getBrush( span=(min(x), max(y)) )
        # obj.graph.plot(x, y, pen = 'black', symbolBrush = brush, symbolSize= 10, name = 'Distance')
        obj.graph.plot(x, y, pen = 'black', symbolBrush = 2, symbolSize= 5, name = 'Distance')
        obj.graph.setMouseEnabled()

        # obj.graph.disableAutoRange()
        ...


def FillTable(tableWidget, listOflistData):

    numRows = len(listOflistData)
    numColunm = len(listOflistData[0])

    tableWidget.setRowCount(numRows)
    tableWidget.setColumnCount(numColunm)
    
    tableWidget.setVerticalHeaderLabels(str(var) for var in (range(numRows)[::-1])) # подписи заголовков таблицы

    # заполнение таблицы по строкам листом из листа
    for Row in range(numRows): # column    
        FillTableRow(Row, tableWidget, listOflistData[Row])


def FillTableRow(numRow, tableWidget, listData):
    # заполнить строку таблицы по столбцам из листа
    for Column in range(len(listData)): # column
        tableWidget.setItem(numRow, Column, QTableWidgetItem(str(listData[Column])))

if __name__ == "__main__":
    print("object_control")