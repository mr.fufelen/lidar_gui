# отрисовка графика через виджет
from pyqtgraph import PlotWidget
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication

from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from qt_lidar_gui_ui import Ui_MainWindow

# from random import randint
from itertools import repeat

# математика
# import numpy as np
import math

# мои модули
import byte_array_convertor as bac
import lidar_parser
import object_control

import lidar_dicts as dc

import random as rnd

# ********************************************************
# подключение слотов на сигналы:

# сигнал ген. когда польз изм. что-то на виджете
# слот - действие по сигналу
# ********************************************************

class MainWindow(QMainWindow, Ui_MainWindow):

    # def __init__(self, *args, **kwargs):
    #     super(MainWindow, self).__init__(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self) # setup от Ui_MainWindow
        self.setWindowTitle("LidarGUI") # название окна

        # переменные
        self.RPM = 0 # rotate per minute lidar
        self.PPM = 0 # points per minute lidar
        self.NUM_points = 0 # колво точек одновременно отображаемых на графике
        self.rx = b'' # хранилище байт с порта
        self.DataToTable = list() # буфер данных для вывода парса пакетов в таблицу
     
        #test
        self.counter = 0

        # настройка порта
        self.serial = QSerialPort()
        self.serial.setBaudRate(115200)

        # список портов в выпадающем списке     
        self.ports = QSerialPortInfo().availablePorts() # список доступных портов
        self.portList = []
        for self.port in self.ports:
            self.portList.append(self.port.portName())
        self.portList.sort(reverse=True)
        print(self.portList)
        self.com_list.addItems(self.portList) # список последовательных портов в выпадающем списке

        self.update_graph_param()

        # подключение слотов к сигналам
        self.OPEN_serial_b.clicked.connect(self.openSerialPort)
        self.CLOSE_serial_b.clicked.connect(self.closeSerialPort)
        self.clear_graph_b.clicked.connect(self.clear_graph)

        self.setRPM_b.clicked.connect(lambda: self.sendLidarPackToSerial("setMotorSpeedRPM", self.enc_speed_set_slider.value())) #  кнопка установки скорости вращения энкодера
        self.enc_speed_set_slider.sliderReleased.connect(lambda: self.sendLidarPackToSerial("setMotorSpeedRPM", self.enc_speed_set_slider.value()))
        self.enc_speed_set_slider.valueChanged.connect(lambda: self.req_speed_rpm_lcd.display(self.enc_speed_set_slider.value()))

        self.serial.readyRead.connect(self.readByteUart) # есть очередной байт на прием

        # подключение таймера на периодический вывод сообщений в порт
        self.timer5sec = QTimer()
        self.timer5sec.timeout.connect(self.timerPrint)
        self.timer5sec.start(5000)

        # # дебажный таймер для отладки парсинга пакета
        # self.timerParseDebug = QTimer()
        # self.timerParseDebug.timeout.connect(self.readByteUart)
        # self.timerParseDebug.start(1)
        # #self.send_serial_b.clicked.connect(self.sendToSerial)


        self.show() # показать интерфейс
        # конец конструктора

    def timerPrint(self):
        # print("hello_from_timer")
        return


    def clear_graph(self):
        # очистка графика
        self.update_graph_param()
        self.graph.clear() # очистка графика для избегания наложения

    def lidar_plot_dot(self,radius_dot,theta_dot):
        # Ntheta = theta*(1000/(2*math.pi)) # перевод угла для адресации массива
        # Ntheta = math.trunc(Ntheta)
        
        # self.radius[Ntheta] = radius

        theta_dot = math.trunc(theta_dot) # округление для адресации массива

        self.radius[theta_dot] = radius_dot

        self.lidar_graph_plot(self.radius,self.theta)
        
    def update_graph_param(self):
        # todo
        self.RPM = 0 # rotate per minute lidar
        self.PPM = 0 # points per minute lidar

        # todo # self.NUM_points = self.PPM/self.RPM
        self.NUM_points = 1000#00

        #resize lists
        self.uart_theta = list(range(self.NUM_points)) # theta 
        self.uart_radius = list(repeat(0, self.NUM_points)) # генерация массива заполненного нулями

    # методы класса
    def closeSerialPort(self):
        # закрыть последовательный порт
        self.serial.close()
        print("port_closed_class")

    def openSerialPort(self):
        # подключиться к последовательному порту
        self.serial.setPortName(self.com_list.currentText())

        if self.serial.open(QIODevice.ReadWrite) == True:
            self.serial.clear() # очистка порта от мусора
            print("port_opened")
        else: print("open_port_failed")



    def sendToSerialTwoHexList(self, TwoHexBytes_list, swapEndian = True):
        # отправка данных в последовательный порт
       # TwoHexBytes_list = [0xFFAA, 0xCCEE, 0xDD12]
        OneHexBytes_list = bac.two_hex_to_one_hex_list(TwoHexBytes_list)

        if swapEndian == True:
            OneHexBytes_list = bac.swap_endian(OneHexBytes_list)

        self.serial.write(bytes(OneHexBytes_list))
        
    def formLidarPack(self, HeaderFromDict, val1, val2, TerminatorFromDict = 'terminator'):
        # формирование пакета для отправки в последовательный порт
        packet = list()

        packet.append(self.dict_set_headers[HeaderFromDict])
        packet.append(val1)       
        packet.append(val2)       
        packet.append(self.dict_with_terminators[TerminatorFromDict])       

        return packet


    def sendLidarPackToSerial(self, HeaderFromDict, val1 = 0, val2 = 0, TerminatorFromDict = 'terminator'):

        packet = self.formLidarPack(HeaderFromDict, val1, val2)

        # packet = bac.swap_endian(packet) # delete

        self.sendToSerialTwoHexList(packet, False)
        


    def setMotorSpeed(self):
        print(self.enc_speed_set_slider.value())
        return

    def calculateTOF(self):
        return

    # ********************************************************************************************************
    def readByteUart(self):
  
        # обработка данных с порта
        Qrx = self.serial.readAll() # чтение с порта
        # накопление байт до 8ми (размер пакета)
        self.rx = self.rx + Qrx.data() # интересует только поле со списком байт

#debug
        # пакет, который приходит через USB с контроллера
        # self.rx = b'\xfe\xff\x18\x08\xc6C\xd6\x01E\x08\xe2J\xe6\x022\x00\xff\x9b\xff'

        if (len(self.rx) >= 19):
        # if (len(self.rx) >= 8): # проверка колва накопленных байт с порта
        
            self.rx = bytearray(self.rx) # чтобы можно было вставить b'\x00'
            self.rx[15:15] = b'\x00' # в позиции coarse encoder только 1 байт - проект написан под 2 байтные поля
            dec_list_rx = bac.byte_arr_to_dec(self.rx)
            self.rx = b'' # пакет преобразован - сброс буфера


            dc.dict_parsed_pack = lidar_parser.parse_pack(dc.dict_parsed_pack, dc.dict_resp_headers, dc.dict_with_terminators, dec_list_rx)

            dc.dict_parsed_pack['body_7'] = 360 * rnd.random() #debug
            
            # работа с таблицей**************************************************************************

            self.tableWidget = QTableWidget()
            self.tableWidget = self.LidarTable

            self.tableWidget.horizontalHeader().setStyleSheet(":section {""background-color: WhiteSmoke ; }")
            self.tableWidget.verticalHeader().setStyleSheet(":section {""background-color: WhiteSmoke ; }")
            
            iterator_on_tab = lidar_parser.k_dict(dc.dict_parsed_pack)
            list_on_tab_vertical_header = list(iterator_on_tab)    
              
            self.tableWidget.setHorizontalHeaderLabels(list_on_tab_vertical_header) # подписи заголовков таблицы

            # формирование двумерного листа для вывода в таблицу
            if (len(self.DataToTable) > 70): # ограничимся 70 строками логов
                del self.DataToTable[0]
            self.DataToTable.append(list(dc.dict_parsed_pack[var] for var in list_on_tab_vertical_header))
        
            object_control.FillTable(self.tableWidget, self.DataToTable[::-1]) # заполнить таблицу


            # todo парсинг на вызов функции изменения колва точек в графике через N = PPM/RPM

            if(dc.dict_parsed_pack['header'] == 'mainPackHeader'):
                

                tof = 4875 * dc.dict_parsed_pack['body_3']/((dc.dict_parsed_pack['body_2'] + 0xFFFF) - dc.dict_parsed_pack['body_1'])

                # tof = 4875 * dec_list_rx[3]/((dec_list_rx[2] + 0xFFFF) - dec_list_rx[1])
                self.enc_tof_lcd.display('{:.03f}'.format(tof))

                parse_dict = {
                    'radius': tof,
                    'theta_coarse':  3.6 * dc.dict_parsed_pack['body_7'], # 3.6
                    'theta_precise': dc.dict_parsed_pack['body_8'],
                }

                dict_to_graph = {
                    'radius': tof,
                    'theta': parse_dict['theta_coarse']
                }                
                
                object_control.update_plot_data(self, dict_to_graph)




            if(dc.dict_parsed_pack['header'] == 'header_graph'):
                # получен пакет для вывода данных на график
                dict_to_graph = {
                    'radius': dc.dict_parsed_pack['body_1'],
                    'theta': dc.dict_parsed_pack['body_2'],
                }
                object_control.update_plot_data(self, dict_to_graph)
                
            if(dc.dict_parsed_pack['header'] == 'respMotorSpeedRPM'):
                self.enc_speed_lcd.display(dc.dict_parsed_pack['body_1'])
                self.enc_speed_rps_lcd.display(dc.dict_parsed_pack['body_1']/60)
                
                
            if(dc.dict_parsed_pack['header'] == 'header_encoder'):
                #обновить значения на индикаторах согласно данным в пакете
                dict_to_screens = {
                    'degree': dc.dict_parsed_pack['body_1'],
                    'speed': dc.dict_parsed_pack['body_2'],
                }
                object_control.update_SCREENS(self, dict_to_screens)



# ********************************************************************************************************



def main():

    app = QApplication([])
    # app = QtWidgets.QApplication([])

    w = MainWindow()

    # app.exec()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()