# py 3.10.0 64-bit


# [253, 555, 123, 254]
# [253, 555, 123, 254]
# [253]
# [555, 123, 254]
# [253, 555]
# [65024]
# [253, 555, 123, 254]

test_list = [253, 555, 123, 254]

# # словарь хранит структуру пакета для энкодера лидара
# encoder_packet_struct = {
#     'header': 253,
#     'degree': 0,
#     'speed': 0,
#     'terminator': 254,

#     'pos_header':0,
#     'pos_degree': 0,
#     'pos_speed': 0,
#     'pos_terminator': 254
# }

def get_key(dict, value):
    # принимает словарь и значение
    # возвращает ключ по значению
    for dict_key, dict_val in dict.items():
        if dict_val == value:
            return dict_key

def parse_header(dict_parsed_pack, dict_with_headers, packet_to_parse):
    # принимает словарь с заголовками, пакет для парса, и словарь, куда положить результат - возвращает словарь с результатом
    # в словарь распарсенного пакета кладется название ключа, которому соответсвует его заголовок (пакета)

    dict_with_headers_values = dict_with_headers.values() # создание листа со значениями словаря
    counter = -1
    for val in packet_to_parse:
        counter += 1
        if (val in dict_with_headers_values):
            # если один из заголовков есть в посылке
            # считаем, что не может быть больше одного заголовка в посылке

            dict_parsed_pack['header'] = get_key(dict_with_headers, val)
            dict_parsed_pack['pos_header'] = counter

            break # заголовок найден - выход из проверки по остальным элементам словаря
    return dict_parsed_pack

def parse_terminator(dict_parsed_pack, dict_with_terminators, packet_to_validate):
    dict_with_terminators_values = dict_with_terminators.values() # создание листа со значениями словаря
    counter = -1
    for val in packet_to_validate:
        counter += 1
        if (val in dict_with_terminators_values):
            # если один из заголовков есть в посылке
            # считаем, что не может быть больше одного заголовка в посылке

            dict_parsed_pack['terminator'] = get_key(dict_with_terminators, val)
            dict_parsed_pack['pos_terminator'] = counter

            break # заголовок найден - выход из проверки по остальным элементам словаря
    return dict_parsed_pack

# проверка на наличие заголовка и терминатора в пакете
def validate_pack(dict_parsed_pack):
# проверка наличия в dict_parsed_pack всех блоков пакета - заголовок, терминатор - если есть - true
# НЕ ПРОВЕРЯЕТ ПРАВИЛЬНОСТЬ ПАРЫ - ЗАГОЛОВОК-ТЕРМИНАТОР - ПРОВЕРКА ТОЛЬКО НА НАЛИЧИЕ ЭТИХ ПОЗИЦИЙ
    if dict_parsed_pack['header'] != -1:
        if dict_parsed_pack['terminator'] != -1:
            # print("packet_validated")
            return True
    return False

# проход по элементам и запоминание позиций по элементам словаря
def set_pos_header_and_terminator_pack(enc_packet_dict, packet_to_parse):
    counter = 0
    for val in packet_to_parse:
        counter += 1
        if val == enc_packet_dict['header']:
            enc_packet_dict['pos_header'] = counter
        if val == encoder_packet_struct['terminator']:
            enc_packet_dict['pos_terminator'] = counter
    return enc_packet_dict # словарь с ключевыми позициями найденными в пакете

# по известным позициям заголовка и терминатора находит тело пакета и разбивает его согласно словарю
def set_pos_body_parts_pack(dict_parsed_pack, packet_to_parse):
    #  индексация элементов массива
    counter = dict_parsed_pack['pos_header']

    # enc_packet_dict['pos_degree'] = pos_header + 1
    # enc_packet_dict['pos_speed'] = pos_header + 2

    # counter += 1
    # if counter < dict_parsed_pack['pos_terminator']:
    #     dict_parsed_pack['pos_body_1'] = counter
    # counter += 1
    # if counter < dict_parsed_pack['pos_terminator']:
    #     dict_parsed_pack['pos_body_2'] = counter
    # counter += 1
    # if counter < dict_parsed_pack['pos_terminator']:
    #     dict_parsed_pack['pos_body_3'] = counter

    counter += 1
    while(counter < dict_parsed_pack['pos_terminator']):
        dict_parsed_pack['pos_body_' + str(counter)] = counter
        counter += 1

    return dict_parsed_pack

def fill_body_parts_pack(dict_parsed_pack, packet_to_parse):
    # заполнение элементов массива по индексам
    # print(len())
    try:
        counter = dict_parsed_pack['pos_header']


        # counter += 1
        # if counter < dict_parsed_pack['pos_terminator']:
        #     dict_parsed_pack['body_1'] = packet_to_parse[dict_parsed_pack['pos_body_1']]
        # counter += 1
        # if counter < dict_parsed_pack['pos_terminator']:
        #     dict_parsed_pack['body_2'] = packet_to_parse[dict_parsed_pack['pos_body_2']]
        # counter += 1
        # if counter < dict_parsed_pack['pos_terminator']:
        #     dict_parsed_pack['body_3'] = packet_to_parse[dict_parsed_pack['pos_body_2']]

        counter += 1
        while(counter < dict_parsed_pack['pos_terminator']):
            dict_parsed_pack['body_' + str(counter)] = packet_to_parse[dict_parsed_pack['pos_body_' + str(counter)]]
            counter += 1

    except IndexError:
        print("IndexError_occured")

    return dict_parsed_pack


def k_dict(dict, forbidd_list = ['header', 'terminator','pos','packet_validated']):# возвращает интератор на элементы словаря, не указанные в forbidd
                for k,v in dict.items():
                    for forbidd_var in forbidd_list:
                        if forbidd_var in k:
                            k_is_validated = False
                            break
                        else:
                            k_is_validated = True
                    if k_is_validated:
                        yield k

def parse_pack(dict_parsed_pack, dict_with_headers, dict_with_terminators, packet_to_parse):

    dict_parsed_pack = parse_header(dict_parsed_pack, dict_with_headers, packet_to_parse)
    dict_parsed_pack = parse_terminator(dict_parsed_pack, dict_with_terminators, packet_to_parse)

    if validate_pack(dict_parsed_pack) == False:
        dict_parsed_pack['packet_validated'] = False
        return dict_parsed_pack
    dict_parsed_pack['packet_validated'] = True
        
    # set_pos_header_and_terminator_pack(enc_packet_dict, packet_to_parse)

    # разложить пакет по индексам в словарь
    dict_parsed_pack = set_pos_body_parts_pack(dict_parsed_pack, packet_to_parse) 

    # заполнить словарь по индексам в пакете
    dict_parsed_pack = fill_body_parts_pack(dict_parsed_pack, packet_to_parse) 

    return dict_parsed_pack

#   прием листа с десятичными значениями
#   его проверка на наличие заголовка и терминатора
#   выделение данных из него
#   возвращение листа для дальнейшей работы программы?

if __name__ == "__main__":

    test_struct = {
    'header': 253,
    'terminator': 254,
    }
    
    print("modul's debug")
    print("parser")

    # validate_pack(encoder_packet_struct, test_list)
    encoder_packet_struct = parse_pack(test_struct, test_list)
    a = 0